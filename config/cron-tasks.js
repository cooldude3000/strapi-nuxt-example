const axios = require('axios')
const dayjs = require('dayjs')

const siteUrl = process.env.SFH_SITE_URL

// TODO: this is a mock up while we wait for a design
const emailTemplate = {
  subject: 'Updates from Standards for Highways',
  text: 'Hi, you have some updates to your saved documents',
  html: `
  <style>
    p, h1, h4 {
      color: #222;
    }
    a {
      color: #4d62e4;
    }
  </style>
  <h1>Hi there <%= user.firstName %></h1>
  <p>There's been some updates that affect your saved documents.</p>

  <%= withdrawing.length > 0 ? '<h4>Withdrawing</h4>' : '' %>
  <% withdrawing.length > 0 ? withdrawing.forEach(function(doc) { %><strong><a href="${siteUrl}/search/html/\${doc.node_id}"><%= doc.title %></a></strong><br><div><%= doc.notes %></div><br><br><% }) : '' %>

  <%= amending.length > 0 ? '<h4>Amending</h4>' : '' %>
  <% amending.length > 0 ? amending.forEach(function(doc) { %><strong><a href="${siteUrl}/search/html/\${doc.node_id}"><%= doc.title %></a></strong><br><div><%= doc.notes %></div><br><br><% }) : '' %>

  <%= partiallyWithdrawing.length > 0 ? '<h4>Partially Withdrawing</h4>' : '' %>
  <% partiallyWithdrawing.length > 0 ? partiallyWithdrawing.forEach(function(doc) { %><strong><a href="${siteUrl}/search/html/\${doc.node_id}"><%= doc.title %></a></strong><br><div><%= doc.notes %></div><br><br><% }) : '' %>

  <%= publishing.length > 0 ? '<h4>Publishing</h4>' : '' %>
  <% publishing.length > 0 ? publishing.forEach(function(doc) { %><strong><a href="${siteUrl}/search/html/\${doc.node_id}"><%= doc.title %></a></strong><br><div><%= doc.notes %></div><br><br><% }) : '' %>

  <br />

  <p>Go to your <a href="${siteUrl}/my-account">Standards For Highways account</a></p>
  <p><small><a href="${siteUrl}/my-account/your-details">Click here to unsubscribe</a></small></p>
  `
}

module.exports = {

  /**
   * Once a day
   * 8.00am
   */
  '0 08 * * *': async ({ strapi }) => {

    if (strapi.server.app.env === 'development') return

    try {
      const {
        withDrawingPubs,
        amendingPubs,
        partiallyWithdrawingPubs,
        publishingPubs,
      } = await strapi.service('api::saved-document.saved-document').latestPublicationsUpdates()

      // Combine all the node ids into an array...
      // $or = [{ node_id: { $eq: String } }]
      const $or = [
        ...withDrawingPubs,
        ...amendingPubs,
        ...partiallyWithdrawingPubs,
        ...publishingPubs,
      ].map(({ nodeId }) => ({ node_id: { $eq: nodeId }, lastNotified: { $null: true } }))

      if ($or.length < 1) return

      // Get all user's who have saved documents that match these node ids...
      // users = [{ firstName: String, email: String, saved_documents: [SavedDocument, ...] }]
      const users = await strapi.db.query('plugin::users-permissions.user').findMany({
        select: ['firstName', 'email', 'emailNotifications'],
        where: {
          $and: [
            {
              $or: [
                { emailNotifications: {$eq: null }},
                { emailNotifications: {$eq: true }},
              ]
            },
            { saved_documents: { $or }},
          ]
        },
        populate: {
          saved_documents: {
            select: ['node_id', 'title', 'lastNotified', 'id'],
            where: { $or },
          }
        },
      })

      if (users.length < 1) return

      // return // DANGER ZONE

      const reducePubsAndSavedDocuments = saved_documents => (docs, { nodeId, notes, title }) => {
        const indx = saved_documents.findIndex(({ node_id }) => node_id === nodeId)

        if (indx > -1) {
          const { node_id, title } = saved_documents[indx]

          return [ ...docs, { node_id, title, notes } ]

        }

        return docs
      } 

      try {
        for ({ firstName, email, saved_documents } of users) {
          if (!email) continue

          await strapi.plugins['email'].services.email.sendTemplatedEmail(
            { to: email },
            emailTemplate,
            {
              user: {
                firstName: !firstName?.length ? '' : firstName,
                email,
              },
              withdrawing: withDrawingPubs.reduce(reducePubsAndSavedDocuments(saved_documents), []),
              amending: amendingPubs.reduce(reducePubsAndSavedDocuments(saved_documents), []),
              partiallyWithdrawing: partiallyWithdrawingPubs.reduce(reducePubsAndSavedDocuments(saved_documents), []),
              publishing: publishingPubs.reduce(reducePubsAndSavedDocuments(saved_documents), []),
            }
          )

          await strapi.db.query('api::saved-document.saved-document').update({
            where: { $or: saved_documents.map(sd => ({ id: { $eq: sd.id } })) },
              data: {
              lastNotified: dayjs().format('YYYY-MM-DD HH:mm:ss'),
            },
          })
        }
      } catch (e) {
        console.log(e)
      }


    } catch (error) {
      console.log(error)
    }

  },
};

