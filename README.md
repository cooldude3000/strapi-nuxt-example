# Herbie - The CMS for DMRB and beyond...

> A Strapi CMS running on Node with a MariaDB database. It provides some of the content for DMRB Nuxt website

## Dev Requirements

- NodeJS + Yarn
- Docker

## Dev Setup

- Create a .env file and paste the example env file from the env vars sections of this document
- Start a database in docker, eg.
  `docker run -e MYSQL_DATABASE=herbie -e MYSQL_USER=herbie -e MYSQL_PASSWORD=herbie -e MYSQL_RANDOM_ROOT_PASSWORD=yes -p 3307:3306 mariadb`
- `yarn`
- `yarn build`
- `yarn develop`

## Environment Variables

Example env vars for a local development instalation

```
JWT_SECRET=8YJ2K00IemMIjywq1u7WFg==
SENDGRID_API_KEY=<find-in-gitlab>
SENDGRID_API_KEY_SENDER=<find-in-gitlab>
API_ENDPOINT=<find-in-gitlab>
API_USERNAME=<find-in-gitlab>
API_PASSWORD=<find-in-gitlab>
SFH_SITE_URL=https://standardsforhighways.co.uk
BASIC_AUTH_USERNAME=<find-in-gitlab>
BASIC_AUTH_PASSWORD=<find-in-gitlab>
```

## Creating a config dump

Some CMS options and field layouts are stored in the database. When you make changes to Strapi's
options you'll likely want to dump these changes and commit them to source ready for deploy
or for the next dev. To make a dump of these settings you need to run:

`yarn cs export`

## Deploying to server enviroments

As part of of the deployment process (and also for local dev) you'll likely want to
import the latest config options in to your database run:

`yarn cs import`

This will probably want to be done after every deploy

## Config export from the UI

You can also do this after running Strapi in the UI. Go to Settings ->
Interface and view a diff of the changes, then choose either Import or Export
from the buttons above.

See [the Strapi Config Sync Plugin](https://github.com/boazpoolman/strapi-plugin-config-sync#-command-line-interface-cli)
docs for more info.
