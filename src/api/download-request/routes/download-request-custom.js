module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/download-request/user/:userId',
      handler: 'download-request.user',
    },
  ]
}

