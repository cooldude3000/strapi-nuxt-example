'use strict';

/**
 * download-request router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::download-request.download-request');
