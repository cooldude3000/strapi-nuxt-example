'use strict'

/**
 *  download-request controller
 */

const { createCoreController } = require('@strapi/strapi').factories

const downloadState = {
  PROCESSING: 'PROCESSING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
}

module.exports = createCoreController('api::download-request.download-request', {
  async user(ctx) {
    const { request } = ctx
    const { params } = request
    const { userId } = params

    try {
      const downloadRequests = await strapi.entityService.findMany('api::download-request.download-request', {
        fields: ['completeDate', 'state', 'jobId', 'searchParams', 'zipUrl', 'validAt', 'errorMessage'],
        filters: {
          users_permissions_user: { id: { $eq: userId } },
        },
        sort: { createdAt: 'DESC' },
        populate: {
          users_permissions_user: { fields: ['id', 'email', 'firstName', 'lastName'] }
        }
      })

      return {
        data: downloadRequests,
      }

    } catch (error) {
      ctx.response.status = 400
      ctx.response.message = error.message
    }
  },
  async create(ctx) {
    const { request } = ctx
    const { body } = request
    const { params, userId, validAt } = body

    try {
      const jobId = await strapi.service('api::saved-document.saved-document').downloadRequest(validAt, params)

      if (!jobId) {
        ctx.response.status = 500
        return
      }

      ctx.request.body = {
        data: {
          jobId,
          state: downloadState.PROCESSING,
          users_permissions_user: userId,
          searchParams: params,
          validAt,
        }
      }

      const res = await super.create(ctx)
      return res

    } catch (error) {
      console.log(error)
    }
  },
  async update(ctx) {
    const { request } = ctx
    const { body, params: requestParams } = request
    const { userId } = body
    const { id } = requestParams

    if (!id) {
      // Bad request
      ctx.response.status = 400
      return
    }

    try {
      const downloadRequest = await strapi.entityService.findOne('api::download-request.download-request', id);
      if (!downloadRequest) {
        // Not found
        ctx.response.status = 404
        return
      }

      const { searchParams: params, validAt } = downloadRequest
      const jobId = await strapi.service('api::saved-document.saved-document').downloadRequest(validAt, params)
      if (!jobId) {
        // Bad request
        ctx.response.status = 400
        return
      }

      ctx.request.body = {
        data: {
          jobId,
          state: downloadState.PROCESSING,
          completeDate: null,
          zipUrl: null,
        }
      }
      const res = await super.update(ctx)
      return res

    } catch (error) {
      console.log(error)
    }
  },
})
