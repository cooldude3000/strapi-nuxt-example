'use strict';

/**
 * html-document-request router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::html-document-request.html-document-request');
