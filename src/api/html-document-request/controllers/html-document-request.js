'use strict';

/**
 *  html-document-request controller
 */

const { createCoreController } = require('@strapi/strapi').factories;
module.exports = createCoreController('api::html-document-request.html-document-request');