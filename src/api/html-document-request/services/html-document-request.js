'use strict';

/**
 * html-document-request service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::html-document-request.html-document-request');
