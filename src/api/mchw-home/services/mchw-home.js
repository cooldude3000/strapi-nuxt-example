'use strict';

/**
 * mchw-home service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::mchw-home.mchw-home');
