/**
 *  mchw-home controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::mchw-home.mchw-home', ({ strapi }) => ({
  async find(ctx) {
    const { query } = ctx;

    const entity = await strapi.entityService.findMany('api::mchw-home.mchw-home', {
        ...query,
        populate: '*'
    });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

    return this.transformResponse(sanitizedEntity);
  }
}));

