'use strict';

/**
 * mchw-home router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::mchw-home.mchw-home');
