'use strict';

/**
 *  cookie-policy controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::cookie-policy.cookie-policy', ({ strapi }) => ({
  async find(ctx) {
    const { query } = ctx

    const entity = await strapi.entityService.findMany('api::cookie-policy.cookie-policy', {
      ...query,
      populate: '*'
    });
    const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

    return this.transformResponse(sanitizedEntity);
  }
}));
