'use strict';

/**
 *  saved-document controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::saved-document.saved-document', {
  async savedDocumentsDetails(ctx) {
    const { params } = ctx
    const { userId } = params

    const { saved_documents } = await strapi.db.query('plugin::users-permissions.user').findOne({
      where: { id: { $eq: userId } },
      populate: { saved_documents: true }
    })

    if (saved_documents.length === 0) return { data: [] }

    const documents = await strapi.service('api::saved-document.saved-document').documentsOptimised(saved_documents.map(d => d.node_id))

    return {
      data: saved_documents.map(savedDocument => {
        return {
          savedDocument,
          resultData: documents.find(({ NODE_ID }) => savedDocument.node_id === NODE_ID)
        }
      })
    }
  },
  async latestUpdates(ctx) {
    const { params } = ctx
    const { userId } = params

    const {
      withDrawingPubs,
      amendingPubs,
      partiallyWithdrawingPubs,
      publishingPubs,
    } = await strapi.service('api::saved-document.saved-document').latestPublicationsUpdates()

    // Combine all the node ids into an array...
    // $or = [{ node_id: { $eq: String } }]
    const $or = [
      ...withDrawingPubs,
      ...amendingPubs,
      ...partiallyWithdrawingPubs,
      ...publishingPubs,
    ].map(({ nodeId }) => ({ node_id: { $eq: nodeId } }))

    const user = await strapi.db.query('api::saved-document.saved-document').findMany({
      select: ['node_id', 'title'],
      where: {
        $and: [
          { users_permissions_user: { id: { $eq: userId } } },
          { $or },
        ],
      },
      populate: { users_permissions_user: { select: ['id'] } }
    })

    return { data: user }

  }

});
