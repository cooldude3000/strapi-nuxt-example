module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/saved-documents/latest/:userId',
      handler: 'saved-document.latestUpdates',
    },
    {
      method: 'GET',
      path: '/saved-documents/details/:userId',
      handler: 'saved-document.savedDocumentsDetails',
    }
  ]
}
