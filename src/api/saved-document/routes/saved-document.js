'use strict';

/**
 * saved-document router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::saved-document.saved-document');
