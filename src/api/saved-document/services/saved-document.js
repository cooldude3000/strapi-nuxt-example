'use strict';

/**
 * saved-document service.
 */

const dayjs = require('dayjs')
const { createCoreService } = require('@strapi/strapi').factories;
const axios = require('axios')

const tses = axios.create({
  baseURL: process.env.API_ENDPOINT,
  auth: {
    username: process.env.API_USERNAME,
    password: process.env.API_PASSWORD,
  }
})

const documentColumns = [
  'NODE_ID',
  'SUITE',
  'TITLE',
  'DOCUMENT_REFERENCE',
  'DISCIPLINE',
  'LIFECYCLE_STAGE',
  'REVISION_ONLY',
  'REVISION',
  'VERSION_NUMBER',
  'VOLUME',
  'SECTION',
  'PART',
  'PUBLIC_NOTES',
  'DATE_OF_ISSUE',
  'DATE_OF_WITHDRAWAL',
  'HAS_PUBLIC_DOCUMENT',
  'SUMMARY',
  'LEGACY_REF',
  'AFFECTED_DOCUMENTS',
  'TYPE_OF_CHANGE',
  'HAS_NAAS',
  'ATTACHMENTS',
]

function extractNodeIdsFromLatestPubs(summaryType, { IAN, DMRB, MCHW }) {
  return [IAN, DMRB, MCHW]
    ?.reduce((acc, standard) => ([...acc, ...(standard[summaryType] || [])]), [])
    ?.reduce((acc, withd) =>  ([...acc, { nodeId: withd.titleData?.nodeId, notes: withd.notes }]), [])
    ?.filter(Boolean) ?? []
}

function formatAndFilterRawDocs(nodeIds) {
  return (acc, { resultData }) => {
    if (nodeIds.includes(resultData.NODE_ID)) {
      return [...acc, resultData]
    }
    return acc
  }
}

module.exports = createCoreService('api::saved-document.saved-document', {
  async documentsOptimised(nodeIds) {
    try {
      // Do a search for the documents by nodeIds...
      const { data: searchResults } = await tses('/search', {
        params: {
          query: nodeIds.map(id => `NODE_ID = '${id}'`).join(' | '),
          columns: documentColumns.join(','),
          pageIndex: 0,
          matchType: 'ALL',
        }
      })

      // Withdrawn docs won't be included in a search so make an array of
      // all the docs we didn't find...
      const { page: searchDocumentsRaw } = searchResults
      const notIncludedNodeIds = nodeIds.reduce((acc, nodeId) => {
        if (searchDocumentsRaw.some(doc => doc.nodeId === nodeId)) return acc
        return [...acc, nodeId]
      }, [])

      // If there aren't any we're good to just send the docs back...
      if (notIncludedNodeIds.length === 0) {
        return searchDocumentsRaw.reduce(formatAndFilterRawDocs(nodeIds), [])
      }

      // If the search has missed some we'll have to fetch each one
      // individually from the /document endpoint...
      const documents = await this.documents(notIncludedNodeIds)

      // Finally merge those documents in with the search results...
      return searchDocumentsRaw.reduce(formatAndFilterRawDocs(nodeIds), documents)

    } catch (error) {
      console.log(error)
    }
  },
  async documents(nodeIds) {
    const documents = []

    try {
      for (const nodeId of nodeIds) {
        const { data: document } = await tses('/documents/' + nodeId, {
          params: {
            columns: documentColumns.join(','),
          }
        })

        documents.push(document)
      }
      return documents

    } catch (error) {
      console.log(error)
    }
  },
  async downloadRequest(validAt, params) {
    try {
      const { data: jobId } = await tses('/attachments/generate-zip', {
        params: {
          query: params.query,
          validAt: dayjs(validAt).valueOf(),
        }
      })

      return jobId

    } catch (error) {
      console.log(error)
      return
    }
  },
  async latestPublicationsUpdates() {
    try {
      const res = await tses('/publication-summaries/latest')

      const { data } = res

      return {
        withDrawingPubs: extractNodeIdsFromLatestPubs('withdrawingSummaries', data),
        amendingPubs: extractNodeIdsFromLatestPubs('amendingSummaries', data),
        partiallyWithdrawingPubs: extractNodeIdsFromLatestPubs('partiallyWithdrawingSummaries', data),
        publishingPubs: extractNodeIdsFromLatestPubs('publishingSummaries', data),
      }
    } catch (error) {
      console.log(error)
    }
  }
});
