const auth = require('basic-auth')

const { BASIC_AUTH_USERNAME, BASIC_AUTH_PASSWORD } = process.env

module.exports = ({ request }, config, { strapi }) => {
  const userPass = auth(request)
  if (!userPass) {
    return false
  }

  const { name, pass } = userPass
  if (!name || !pass) {
    return false
  }

  if (
    name !== BASIC_AUTH_USERNAME ||
    pass !== BASIC_AUTH_PASSWORD
  ) {
    return false
  }

  return true
};
