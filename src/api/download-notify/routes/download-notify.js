module.exports = {
  routes: [
    {
      method: 'PUT',
      path: '/download-notify',
      handler: 'download-notify.put',
      config: {
        auth: false,
        policies: ['basic-auth']
      },
    },
  ],
}
