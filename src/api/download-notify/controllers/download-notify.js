const dayjs = require('dayjs')

const siteUrl = process.env.SFH_SITE_URL

const emailTemplateSuccess = {
  subject: 'Downloads from Standards for Highways',
  text: 'Hi, your download has completed!',
  html: `
  <style>
    p, h1, h4 {
      color: #222;
    }
    a {
      color: #4d62e4;
    }
  </style>
  <h1>Hi there <%= user.firstName %></h1>
  <p>Your download request has completed.</p>
  <p>Click <a href="<%= zipUrl %>">here</a> to download your documents.</p>
  <br />

  <p>Go to your <a href="${siteUrl}/my-account">Standards For Highways account</a></p>
  <p><small><a href="${siteUrl}/my-account/your-details">Click here to unsubscribe</a></small></p>
  `
}

const emailTemplateError = {
  subject: 'Downloads from Standards for Highways',
  text: 'There was an error with your zip download',
  html: `
  <style>
    p, h1, h4 {
      color: #222;
    }
    a {
      color: #4d62e4;
    }
  </style>
  <h1>Hi <%= user.firstName %></h1>
  <p>Unfortunately there was an error creating your zip file</p>
  <h6>Error message</h6>
  <p><%= errorMessage %></p>
  <br />

  <p>Go to your <a href="${siteUrl}/my-account">Standards For Highways account</a></p>
  <p><small><a href="${siteUrl}/my-account/your-details">Click here to unsubscribe</a></small></p>
  `
}

module.exports = {
  async put(ctx, next) {
    const { request } = ctx
    const { zipUrl, jobId, status, errorMessage } = request.body

    if (!zipUrl || !jobId || !status) {
      console.log('One of zipUrl, jobId or status is missing')
      ctx.response.status = 400
      return
    }

    try {
      const downloadRequest = await strapi.db.query('api::download-request.download-request').findOne({
        where: {
          $and: [
            { jobId: { $eq: jobId } },
            {
              $or: [
                { users_permissions_user: { emailNotifications: { $eq: null }} },
                { users_permissions_user: { emailNotifications: { $eq: true }} },
              ]
            }
          ],
        },
        populate: {
          users_permissions_user: {
            select: ['firstName', 'email', 'emailNotifications'],
          },
        },
      })

      if (!downloadRequest) {
        console.log('No downloadRequest found')
        // Not found
        ctx.response.status = 404
        return
      }

      const {
        // TODO
        // validAt,
        id,
        users_permissions_user,
      } = downloadRequest

      // If there's no user, delete this record, it's orphaned
      if (!users_permissions_user) {
        console.log('No user found associated with jobId: ', jobId)
        ctx.response.status = 500
        strapi.entityService.delete('api::download-request.download-request', id)
        return
      }

      await strapi.entityService.update('api::download-request.download-request', id, {
        data: {
          state: status,
          zipUrl,
          completeDate: dayjs().format('YYYY-MM-DD HH:mm:ss'),
          errorMessage,
        }
      })

      const { firstName, email, emailNotifications } = users_permissions_user

      if (typeof emailNotifications === 'boolean' && emailNotifications === false) {
        ctx.response.status = 200
        return
      }

      await strapi.plugins['email'].services.email.sendTemplatedEmail(
        { to: email },
        status === 'SUCCESS' ? emailTemplateSuccess : emailTemplateError,
        {
          user: {
            firstName: !firstName?.length ? '' : firstName,
            email,
          },
          // TODO...
          // validAt,
          zipUrl,
          errorMessage,
        }
      )

      // OK
      ctx.response.status = 200

    } catch (error) {
      console.log(error)
      // Server error
      ctx.response.status = 500

    }
  },
}
